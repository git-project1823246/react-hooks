
// src/components/Courses.js
import React from 'react';
import styled from 'styled-components';
import CourseCard from '../CourseCard/CourseCard';
import EmptyCourseList from '../EmptyCourseCard/EmptyCourseCard';
import Button from '../Button/Button';

const CoursesContainer = styled.div`
  padding: 20px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 20px 100px 0px 100px; // Соответствует отступам карточки
`;

const Courses = ({ courses, authors }) => {
  return (
    <CoursesContainer>
      <ButtonContainer>
        <Button buttonText="Add New Course" onClick={() => {}} />
      </ButtonContainer>
      {courses.length === 0 ? (
        <EmptyCourseList />
      ) : (
        courses.map((course) => <CourseCard key={course.id} course={course} authors={authors} />)
      )}
    </CoursesContainer>
  );
};

export default Courses;
